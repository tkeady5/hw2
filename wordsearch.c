/* 
 * Thomas Keady
 * 600.120 Intermediate Programming
 * 2/16/15
 * Assignment 2
 * tkeady1
 * tkeady1@jhu.edu
 * 516-729-9535
 */



#include "wordsearch.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#define MAXSZ 10
	
// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.

//searches vertically upwards
bool find_word_up(char grid[][MAXSZ], int n, char w[], int i, int j) {
	//printf("im in find word up\n");
	
	//	need to cast to int because strlen() returns unsigned int
	if (i - (int)strlen(w) +1 < 0 && n) {		//if the length of the word means it will go out of bounds...
		return false;				//then it cant be there			
	} else {
		//tracker moves through the grid going up starting at the
		//second letter and moves relative to that letter until it reaches
		//the end of the string (if it is there). if there ever isnt a
		//match, it returns false, otherwise the end of the function is
		//reached and it returns true  
		for (int tracker = 1; tracker < (int)strlen(w); tracker++) {
			//printf("tracker = %d\nw[t]: %c\n", tracker, w[tracker]);
			if (w[tracker] != grid[i-tracker][j]) {
				return false;
			}
		}
	
	}
	
	
	return true;

}


bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j) {

	if (i + (int)strlen(w) > n) {			//same check as in the beginning of find_word_up
		return false;
	} else {
		//tracker moves through the grid going down starting at the
		//second letter and moves relative to that letter until it reaches
		//the end of the string (if it is there). if there ever isnt a
		//match, it returns false, otherwise the end of the function is
		//reached and it returns true
		for (int tracker = 1; tracker < (int)strlen(w); tracker++) {
			//printf("tracker = %d\nw[t]: %c\n", tracker, w[tracker]);
			if (w[tracker] != grid[i+tracker][j]) {
				return false;
			}
		}
	
	}
	return true;
	
}


bool find_word_left(char grid[][MAXSZ], int n, char w[], int i, int j) {
	
	if (j - (int)strlen(w) +1 < 0 && n) {           //same check as in the beginning of find_word_up
		return false;
	} else {
		//tracker moves through the grid going left starting at the
		//second letter and moves relative to that letter until it reaches
		//the end of the string (if it is there). if there ever isnt a
		//match, it returns false, otherwise the end of the function is
		//reached and it returns true
		for (int tracker = 1; tracker < (int)strlen(w); tracker++) {
			//printf("tracker = %d\nw[t]: %c\n", tracker, w[tracker]);
			if (w[tracker] != grid[i][j-tracker]) {
				return false;
			}
		}
	
	}
	return true;
}


bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j) {
	
	if (j + (int)strlen(w) > n) {		//same check as in the beginning of find_word_up
		return false;
	} else {
		//tracker moves through the grid going right starting at the
		//second letter and moves relative to that letter until it reaches
		//the end of the string (if it is there). if there ever isnt a
		//match, it returns false, otherwise the end of the function is
		//reached and it returns true		
		for (int tracker = 1; tracker < (int)strlen(w); tracker++) {
			//printf("tracker = %d\nw[t]: %c\n", tracker, w[tracker]);
			if (w[tracker] != grid[i][j+tracker]) {
				return false;
			}
		}
	
	}
	return true;
	
}
 

void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]) {
	//printf("i did it!\nchar w: %s\n", w);
	/*
	for (int k = 0; k < n; k++) {
		for (int l = 0; l < n; l++) {
			if (grid[k][l] == w[		//leftover print grid
		}					
		printf("\n");
	}*/
	
	FILE* of = fopen("matches.txt", "w");
	
	char s;
	fscanf(outfile, "%c\n", &s);			//to make a warning go away :)
	
	bool didWeFindIt = false;			//we havnt found it yet
	//double for loop to go through the grid, looking for a match of the first character in the word
	for (int k = 0; k < n; k++) {					
		for (int l = 0; l < n; l++) {
			//printf("k: %d\tl: %d\n", k, l);
			if (grid[k][l] == w[0]) {			//if we get a match...
				if (find_word_right(grid, n, w, k, l)) {	//look in all 4 directions
					printf("%s %d %d R\n", w, k, l);	//(corresponding print statements for each direction)
					fprintf(outfile, "%s %d %d D\n", w, k, l);	//print matches to file
					didWeFindIt = true;		//if we found it then we store that fact here!
				} 
				if (find_word_left(grid, n, w, k, l)) {
					printf("%s %d %d L\n", w, k, l);
					fprintf(of, "%s %d %d D\n", w, k, l);
					didWeFindIt = true;
				}
				if (find_word_up(grid, n, w, k, l)) {
					printf("%s %d %d U\n", w, k, l);
					fprintf(of, "%s %d %d D\n", w, k, l);
					didWeFindIt = true;
				}
				if (find_word_down(grid, n, w, k, l)) {
					printf("%s %d %d D\n", w, k, l);
					fprintf(of, "%s %d %d D\n", w, k, l);
					didWeFindIt = true;
				}
			
				
				
			}
		}
		
	}
	if (!didWeFindIt) {
		printf("%s - not found\n", w);	//if we didnt find it (didWeFindIt is still false) then print the "- not found" message :(
	}
	//printf("Exiting write_all\n");
		
	
	
}




int load_grid(FILE* infile, char outgrid[][MAXSZ]) {
	char c; 				//where each character read from file will go
	int i = 0, j = 0;			//i = rows, j = cols
	int firstj = 0;				//holds the length of the first row
	int firstTime = 1;			//only lets us set firstj once (the first time)
	int lastCharNewline = 0;		//holds if the last character was a newline (since there are 2 at end then can see if 2 in a row)
	while (fscanf(infile, "%c", &c) != EOF) { 	
		
		if (c != '\n') {		//if we get a letter...
			outgrid[i][j] = tolower(c);		//put it in the grid
			j++;			//the next slot in the row
			if (j == 11) {		//if the row is now too big (check here to avoid seg fault *phew*)
				printf("One grid line (%d) went over 10 characters\n", i);	//and let us know if that happened by returning 0
				return 0;
			}
			lastCharNewline = 0;	//take note that this character was not a newline
			
		} else {			//if it is a newline
			
			if (firstTime) {	//if its the first time we got a newline...
				firstj = j;	//record how many characters there were (used to check the number for the rest of the lines)
				firstTime = 0;
				j = 0;		//reset column counter for next row
				
			} else {		//if it wasnt the first time...
				if (j != firstj && !lastCharNewline) {	//if the length of this row doesnt match the length of the first row (and its not cause the last row was a newline
					printf("Grid rows have different numbers of characters\n");	//then let us know
					return 0;		
				} else if (lastCharNewline) {		//but if the last one was a newline (so we got 2 in a row)...
					break;				//then exit cause we ignore everything after that
				}
				
				j = 0;				//if neither of those is the case dont forget to reset the column counter
			}
			lastCharNewline = 1;		//record that we got a newline this time...
			i++; 				//so we move on to the next row
			if (i == 11) {			//if theres one after the 10th row...
				printf("Too many rows\n"); //kill it before seg fault!!!
				return 0;
			}
			
		
		}	// exit the if statement where we read check what the character is
		
	}		// exit while loop where we read file
	
	
	j = firstj;		//by-product of resetting the column counter (j) after each row is that it is 0 at the end, so fix that
	//printf("got here 1\ni: %d\nj: %d\nfirstj: %d\n", i, j, firstj);  //leftover from testing 
	
	if (i != j) {				//make sure the number of rows equals the number of columns
		printf("Array not square\n");
		
		return 0;
	}
	/*
	for (int k = 0; k < i; k++) {
		for (int l = 0; l < j; l++) {
			printf("%c", outgrid[k][l]);		//this leftover chunk prints back the grid
		}
		printf("\n");
	}*/
	
	return i; 	//return number of rows in grid

}





 	

