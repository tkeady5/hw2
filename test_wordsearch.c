#include "wordsearch.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#define MAXSZ 10
/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}


/*
 * writes out a hard-coded grid to a file;
 * uses read_grid to read in the grid;
 * test that the loaded grid is correct
 */
void test_read_grid() {
	
	char grid[MAXSZ][MAXSZ];
	//int count = 0;
	
	
	FILE* fptr = fopen("test1.txt", "w");	//make a file
	fprintf(fptr, "\n");			//put the grid to check on in it
	fclose(fptr);				//close it
	
	FILE* open = fopen("test1.txt", "r");	//reopen as readable
	
	assert(load_grid(open, grid) == 0);	//assert that it is the case
	//printf("%d\n", count++);	
	
			// 6 tests, 1, 3 and 6 check the invalid grid parameters
			// 2, 4 & 5 check different grid sizes (1x1, 4x4 and 10x10, respectively

			// i use the same pre-written files for other tests too
			
	
	fptr = fopen("test2.txt", "w");
	fprintf(fptr, "a\n");
	fclose(fptr);
						//test 2
	open = fopen("test2.txt", "r");
	
	assert(load_grid(open, grid) == 1);
	//printf("%d\n", count++);
	
	
	
		
	fptr = fopen("test3.txt", "w");
	fprintf(fptr, "PooP\noPoo\nooPo\nPooPoops\n");
	fclose(fptr);	
						//test 3
	open = fopen("test3.txt", "r");
	
	assert(load_grid(open, grid) == 0);
	//printf("%d\n", count++);
	

	
	fptr = fopen("test4.txt", "w");
	fprintf(fptr, "PooP\noPoo\nooPo\nPooP\n");
	fclose(fptr);
						//test 4
	open = fopen("test4.txt", "r");
	
	assert(load_grid(open, grid) == 4);
	//printf("%d\n", count++);
	
	
	
	
	fptr = fopen("test5.txt", "w");
	fprintf(fptr, "1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n1234567890\n");
	fclose(fptr);
						//test 5
	open = fopen("test5.txt", "r");
	
	assert(load_grid(open, grid) == 10);
	//printf("%d\n", count++);
	
	
	fptr = fopen("test6.txt", "w");
	fprintf(fptr, "1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A\n1234567890A");
	fclose(fptr);;

	open = fopen("test6.txt", "r");
						//test 6
	assert(load_grid(open, grid) == 0);
	//printf("%d\n", count++);
		
	
	
}

void test_find_forward() {  
	
	char grid[MAXSZ][MAXSZ];
	//int count = 0;
	
	FILE* open = fopen("test4.txt", "r");	//bring in this grid to demonstrate 
	int n = load_grid(open, grid);		//efficacy of find_word functions
	
	char w[4] = {'p', 'o', 'o', 'p'};		//i found this to be a convenient example because
	assert(find_word_right(grid, n, w, 0, 0));	//the arrangement shows that the function will not
	//printf("%d\n", count++);			//find a match until all the letters in the word are
	assert(!find_word_right(grid, n, w, 0, 3));	//matched; in other words it will report no match if
	//printf("%d\n", count++);			//just the last character in a word is absent
	assert(!find_word_right(grid, n, w, 1, 1));
	//printf("%d\n", count++);			//the palendrome provided nice symmetry and testing
	assert(!find_word_right(grid, n, w, 2, 2));	//on the corners in multiple directions
	//printf("%d\n", count++);
	assert(!find_word_right(grid, n, w, 3, 3));
	//printf("%d\n", count++);
	assert(find_word_right(grid, n, w, 3, 0));
	//printf("%d\n", count++);
	
	char x[3] = {'p', 'e', 'e'};			//a word not present in the grid will 
	assert(!find_word_right(grid, n, x, 0, 0));	//never be found
	//printf("%d\n", count++);
	assert(!find_word_right(grid, n, x, 1, 1));
	//printf("%d\n", count++);
	assert(!find_word_right(grid, n, x, 3, 3));
	//printf("%d\n", count++);
	
}
void test_find_backward() { 
		
	char grid[MAXSZ][MAXSZ];
	//int count = 0;
	
	FILE* open = fopen("test4.txt", "r");
	int n = load_grid(open, grid);		//same deal as before
	
	char w[4] = {'p', 'o', 'o', 'p'};	
	assert(!find_word_left(grid, n, w, 0, 0));	
	//printf("%d\n", count++);		
	assert(find_word_left(grid, n, w, 0, 3));	//but now this one will work because searching to the left
	//printf("%d\n", count++);		
	assert(!find_word_left(grid, n, w, 1, 1));	//these still wont
	//printf("%d\n", count++);		
	assert(!find_word_left(grid, n, w, 2, 2));
	//printf("%d\n", count++);
	assert(find_word_left(grid, n, w, 3, 3));
	//printf("%d\n", count++);
	assert(!find_word_left(grid, n, w, 3, 0));
	//printf("%d\n", count++);
	
	char x[3] = {'p', 'e', 'e'};			//a word not present in the grid will 
	assert(!find_word_left(grid, n, x, 0, 0));	//never be found
	//printf("%d\n", count++);
	assert(!find_word_left(grid, n, x, 1, 1));
	//printf("%d\n", count++);
	assert(!find_word_left(grid, n, x, 3, 3));
	//printf("%d\n", count++);

}
void test_find_up() { 
	
	char grid[MAXSZ][MAXSZ];
	//int count = 0;
	
	FILE* open = fopen("test4.txt", "r");
	int n = load_grid(open, grid);		//same deal as before
	
	char w[4] = {'p', 'o', 'o', 'p'};	
	assert(!find_word_up(grid, n, w, 0, 0));	
	//printf("%d\n", count++);		
	assert(!find_word_up(grid, n, w, 0, 3));	
	//printf("%d\n", count++);		
	assert(!find_word_up(grid, n, w, 1, 1));
	//printf("%d\n", count++);		
	assert(!find_word_up(grid, n, w, 2, 2));
	//printf("%d\n", count++);
	assert(find_word_up(grid, n, w, 3, 3));	//now these two work cause they are the bottom row
	//printf("%d\n", count++);
	assert(find_word_up(grid, n, w, 3, 0));
	//printf("%d\n", count++);
	
	char x[3] = {'p', 'e', 'e'};			//a word not present in the grid will 
	assert(!find_word_up(grid, n, x, 0, 0));	//never be found
	//printf("%d\n", count++);
	assert(!find_word_up(grid, n, x, 1, 1));
	//printf("%d\n", count++);
	assert(!find_word_up(grid, n, x, 3, 3));
	//printf("%d\n", count++);
	
}
void test_find_down() {
	
	char grid[MAXSZ][MAXSZ];
	//int count = 0;
	
	FILE* open = fopen("test4.txt", "r");
	int n = load_grid(open, grid);		//same deal as before
	
	char w[4] = {'p', 'o', 'o', 'p'};	
	assert(find_word_down(grid, n, w, 0, 0));//now these two work cause they are the top row	
	//printf("%d\n", count++);		
	assert(find_word_down(grid, n, w, 0, 3));	
	//printf("%d\n", count++);		
	assert(!find_word_down(grid, n, w, 1, 1));
	//printf("%d\n", count++);		
	assert(!find_word_down(grid, n, w, 2, 2));
	//printf("%d\n", count++);
	assert(!find_word_down(grid, n, w, 3, 3));
	//printf("%d\n", count++);
	assert(!find_word_down(grid, n, w, 3, 0));
	//printf("%d\n", count++);
	
	char x[3] = {'p', 'e', 'e'};			//a word not present in the grid will 
	assert(!find_word_down(grid, n, x, 0, 0));	//never be found
	//printf("%d\n", count++);
	assert(!find_word_down(grid, n, x, 1, 1));
	//printf("%d\n", count++);
	assert(!find_word_down(grid, n, x, 3, 3));
	//printf("%d\n", count++);
	
}
void test_write_all_matches() {
	/*printf("got here 1\n");
	
	FILE* fptr = fopen("grid.txt", "w");
	fprintf(fptr, "abcd\nabcd\nahid\nabcd\n");
	fclose(fptr);
	
	printf("got here 2\n");
	fptr = fopen("grid.txt", "r");
	FILE* write = fopen("compare.txt", "w");
	FILE* proper = fopen("expectedOutput.txt", "w"); 	//couldnt get this to work
	
	printf("got here 3\n");
	char grid[MAXSZ][MAXSZ];
	int n = load_grid(fptr, grid);
	fptr = fopen("grid.txt", "r");
	printf("got here 5\n");
	char z[2] = {'h', 'i'};
	write_all_matches(fptr, grid, n, z);
	
	*/
	
	
}


int main(void) {
    printf("Testing fileeq...\n");
    test_fileeq();
    printf("Passed fileeq tests.\n");

    printf("Running wordsearch tests...\n");
    test_read_grid();
    test_find_forward();
    test_find_backward();
    test_find_up();
    test_find_down();
    test_write_all_matches();
    printf("Passed wordsearch tests.\n");
}
