/* 
 * Thomas Keady
 * 600.120 Intermediate Programming
 * 2/16/15
 * Assignment 2
 * tkeady1
 * tkeady1@jhu.edu
 * 516-729-9535
 */


#include "wordsearch.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

#define MAXSZ 10

int main(int argc, char* argv[]) { 
	if (argc == 1) {
		printf("Usage: hw2 word-grid.txt\n");
		return 1;	//make sure they give it a input file
	} 
	//printf("%s\n", argv[1]);
	
	FILE* infile = fopen(argv[1], "r"); //open the file they give
	
	char grid[MAXSZ][MAXSZ];
	int n = load_grid(infile, grid);	//put the grid in grid
	
	if (n == 0) {
		printf("Grid problem\n");	//just to check
		return 1;
	}
	
	char w[200];		//assuming the user wont ask for words totalling > 200 characters
	
	printf("Enter words to search for: ");	//prompt
	char single[n];				//for "single word", as in this will store an individual word to feed to searching functions
	//unsigned int stringCounter = 0,
	int  wordCounter = 0;			//stores index of the characters in an individual word 
	
	//this loop will go over the users input
	while (scanf("%s", w) != EOF) {		//while they dont enter ctrl+D
		
		for (unsigned int stringCounter = 0; stringCounter < strlen(w); stringCounter++) {	//stringcounter goes through the entire input
			
			if (w[stringCounter] != ' ' && w[stringCounter] != '\n') {	//as long as it isnt whitespace...
				single[wordCounter] = tolower(w[stringCounter]);			//put it into the single word array
				wordCounter++;
			} 
		}
		
		
		//printf("exited for, about to enter write_all...\n");
		write_all_matches(infile, grid, n, single);	//the for exits when you reach the end of the word, so then start looking for it
		wordCounter--;					//wordCounter is incremented after the arrays are set, so need to shorten it to use as index
		while (wordCounter >= 0) {			//start counting down...
			//printf("\tw before: %c\n", single[wordCounter]);
			single[wordCounter] = 0;		//and clear the array for the next word
			//printf("\tw after: %c\n", single[wordCounter]);
			wordCounter--;
		}
		//printf("%s\n", single);
		wordCounter = 0;		//reset the counter
		
	} //while loop (and program) will only end 
	
	
	return 0;
}











